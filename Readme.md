# Docker for Laravel Project

## Ingredients:
- Dockerfile based on php-fpm 7.2
- nginx:alpine
- mysql:5.7.2

## Quick Setup
- Clone this repository
- Execute: `docker-compose up -d`

## Adding / Removing php-extension
- Edit `Dockerfile`
- Add your additional extensions at line that contains `docker-php-ext-install`
- Rebuild image: `docker-compose build`
- Reload new builded image: `docker-compose up -d`

## Change PHP Version
- Edit `Dockerfile`
- Change the php-fpm version at the top of the file
- Rebuild image: `docker-compose build`
- Reload new builded image: `docker-compose up -d`

## Running `composer` Command
- `docker-compose exec app composer install`

## Running `artisan` command
- `docker-compose exec app php artisan`
