FROM php:7.3-fpm

COPY ./99fixbadproxy /etc/apt/apt.conf.d/99fixbadproxy


RUN apt-get update && apt-get install -y wget curl libxml2-dev libssl-dev zlib1g-dev apt-transport-https lsb-release ca-certificates libpng-dev libturbojpeg0 libjpeg-dev libzlib-dev \
    && docker-php-ext-configure gd --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install mbstring iconv xml pdo_mysql phar zip gd exif

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
